package gla

import (
	"image"
	"image/color"

	"bitbucket.org/clayts/geometry"
	"github.com/go-gl/gl/all-core/gl"
)

type Texture struct {
	gl     uint32
	pixels geometry.AAB
	loadF  func(t *Texture)
}

func (t *Texture) BoundingBox() geometry.AAB { return t.pixels }

func NewTextureFromFile(file string, smooth bool) *Texture {
	t := &Texture{0, GetImageBoundingBox(file), func(t *Texture) {
		gl, err := newGLTexture(GetImage(file), smooth, Repeat)
		if err != nil {
			panic("bad texture")
		}
		t.gl = gl
		t.loadF = nil
	}}
	return t
}

func (t *Texture) Replace(t2 *Texture) {
	if t.loadF == nil {
		gl.DeleteFramebuffers(1, &t.gl)
	}
	t.gl = t2.gl
	t.loadF = t2.loadF
	t.pixels = t2.pixels
}

func NewTextureFromRGBA(rgba *image.RGBA, smooth bool) *Texture {
	t := &Texture{0, i2eBox(rgba.Bounds()), func(t *Texture) {
		gl, err := newGLTextureFromRGBA(rgba, true, Discard)
		if err != nil {
			panic("bad texture")
		}
		t.gl = gl
		t.loadF = nil
	}}
	return t
}

func NewTextureFromColour(r, g, b, a float32, smooth bool) *Texture {
	t := &Texture{0, geometry.NewAABFromLBRT(0, 0, 1, 1), func(t *Texture) {
		// fmt.Println("loading texture from colour", uint8(r*255), uint8(g*255), uint8(b*255), uint8(a*255))
		img := &image.Uniform{
			color.RGBA{
				uint8(r * 255),
				uint8(g * 255),
				uint8(b * 255),
				uint8(a * 255),
			},
		}

		gl, err := newGLTexture(img, smooth, Repeat)
		if err != nil {
			panic("bad texture")
		}
		t.gl = gl
		t.loadF = nil
	}}
	return t
}

func NewTextureFromColourUint8(r, g, b, a uint8, smooth bool) *Texture {
	t := &Texture{0, geometry.NewAABFromLBRT(0, 0, 1, 1), func(t *Texture) {
		// fmt.Println("loading texture from colour", uint8(r*255), uint8(g*255), uint8(b*255), uint8(a*255))
		img := &image.Uniform{
			color.RGBA{
				r,
				g,
				b,
				a,
			},
		}

		gl, err := newGLTexture(img, smooth, Repeat)
		if err != nil {
			panic("bad texture")
		}
		t.gl = gl
		t.loadF = nil
	}}
	return t
}

func NewBlankTexture(size geometry.Vector, smooth bool) *Texture {
	t := &Texture{0, geometry.NewAABFromLBRT(0, 0, size.X(), size.Y()), func(t *Texture) {
		img := image.NewRGBA(image.Rect(0, 0, int(size.X()), int(size.Y())))
		gl, err := newGLTextureFromRGBA(img, smooth, Repeat)
		if err != nil {
			panic("bad texture")
		}
		t.gl = gl
		t.loadF = nil
	}}
	return t
}

func NewProceduralTexture(size geometry.Vector, smooth bool, procedure func()) *Texture {
	t := &Texture{0, geometry.NewAABFromLBRT(0, 0, size.X(), size.Y()), func(t *Texture) {
		img := image.NewRGBA(image.Rect(0, 0, int(size.X()), int(size.Y())))
		gl, err := newGLTextureFromRGBA(img, smooth, Repeat)
		if err != nil {
			panic("bad texture")
		}
		t.gl = gl
		t.loadF = nil
		t.Capture(procedure)
	}}
	return t
}

func (t *Texture) GL() uint32 {
	if t.loadF != nil {
		t.loadF(t)
	}
	return t.gl
}

//
// type texSpec struct {
// 	tex    string
// 	smooth bool
// 	wrap   TextureOverflowOperation
// }

func i2eBox(bounds image.Rectangle) geometry.AAB {

	min, max := bounds.Min, bounds.Max
	return geometry.NewAABFromLBRT(float32(min.X), float32(min.Y), float32(max.X), float32(max.Y))
}

var imageLibrary map[string]image.Image

func GetImageBoundingBox(file string) geometry.AAB {
	return i2eBox(GetImage(file).Bounds())
}

func GetImageAspect(file string) geometry.AAB {
	box := GetImageBoundingBox(file)
	mw := box.RadiusX()
	if box.RadiusY() < mw {
		mw = box.RadiusY()
	}
	scale := box.Radius().OverXY(mw)
	return geometry.NewAABFromXYRadius(0, 0, scale)
}

// func GetImagePixelBlockTransform(file string, blockSizePx int, )
// x, y := float32(24+float32(rand.Intn(7))), float32(5)
// blockSize := float32(16)
// src := geometry.NewAABToAABTransform(
// 	fire.Geometry().BoundingBox(),
// 	geometry.NewAABFromLBRT(
// 		x*blockSize,
// 		y*blockSize,
// 		(x+1)*blockSize,
// 		(y+2)*blockSize,
// 	),
// ).FollowedBy(canvas.GetImagePixelTransform("tileset.png"))

var frameBuffer uint32

func FrameBuffer() uint32 {
	if frameBuffer == 0 {
		// fmt.Println("generating framebuffer")
		gl.GenFramebuffers(1, &frameBuffer)
	}
	return frameBuffer
}

// func CreateTexture(file string, smooth bool, wrap TextureOverflowOperation, size geometry.Vector) {
//
// }

var captureStack []func()

func stack(setup func(), f func()) {
	captureStack = append(captureStack, setup)
	setup()
	f()
	captureStack = captureStack[:len(captureStack)-1]
	if len(captureStack) > 0 {
		next := captureStack[len(captureStack)-1]
		next()
	}
}

func (tex *Texture) Capture(f func()) {
	setup := func() {
		sz := tex.pixels.Size()
		gl.Viewport(0, 0, int32(sz.X()), int32(sz.Y()))
		gl.BindFramebuffer(gl.FRAMEBUFFER, FrameBuffer())
		gl.FramebufferTexture(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, tex.GL(), 0)
		gl.DrawBuffer(gl.COLOR_ATTACHMENT0)
	}
	stack(setup, f)
}

func (w *Window) Capture(destination geometry.AAB, f func()) {
	setup := func() {
		realDst := geometry.NewAABFromLBRT((destination.L()/2)+0.5, (destination.B()/2)+0.5, (destination.R()/2)+0.5, (destination.T()/2)+0.5)
		dstSize := realDst.Size()

		winSize := w.Size()

		gl.BindFramebuffer(gl.FRAMEBUFFER, 0)
		gl.Viewport(int32(realDst.L()*winSize.X()), int32(realDst.B()*winSize.Y()), int32(dstSize.X()*winSize.X()), int32(dstSize.Y()*winSize.Y()))
	}
	stack(setup, f)
}

func GetImage(file string) image.Image {
	if imageLibrary == nil {
		imageLibrary = make(map[string]image.Image)
	}
	if t, ok := imageLibrary[file]; ok {
		return t
	}
	var img image.Image
	var err error
	img, err = NewImage(file)
	if err != nil {
		panic(err)
	}
	imageLibrary[file] = img
	return img
}

//
// var textureLibrary map[texSpec]uint32
//
// //GetTexture loads a texture if it needs loading and returns the gl uint reference to the texture
// func GetTexture(file string, smooth bool, wrap TextureOverflowOperation) uint32 {
// 	texs := texSpec{file, smooth, wrap}
// 	if textureLibrary == nil {
// 		textureLibrary = make(map[texSpec]uint32)
// 	} else if t, ok := textureLibrary[texs]; ok {
// 		return t
// 	}
// 	img := GetImage(file)
// 	t, err := NewTexture(img, smooth, wrap)
// 	if err != nil {
// 		panic(err)
// 	}
// 	textureLibrary[texs] = t
// 	return t
// }

//
// func SetTexture(file string, smooth bool, wrap TextureOverflowOperation, tex uint32) {
// 	texs := texSpec{file, smooth, wrap}
// 	if textureLibrary == nil {
// 		textureLibrary = make(map[texSpec]texData)
// 	}
// 	textureLibrary[texs] = tex
// }
