package gla

import (
	"fmt"
	"image"
	"image/draw"
	"os"
	"runtime"
	"strings"
	"time"

	"bitbucket.org/clayts/geometry"
	"github.com/go-gl/gl/all-core/gl"
	"github.com/go-gl/glfw/v3.1/glfw"
	"github.com/go-gl/mathgl/mgl32"
)

const MaxDepth = 0.99999991
const FloatSize = 4

type Program struct {
	program                                      uint32
	uniforms                                     map[string]int32
	attributes                                   map[string]uint32
	output                                       string
	vertexShader, geometryShader, fragmentShader string
	compiled                                     bool
}

type TextureOverflowOperation int

const (
	Discard TextureOverflowOperation = iota
	Mirror
	Repeat
)

func textBetween(str, prefix, suffix string) []string {
	spl := strings.Split(str, prefix)[1:]
	for i, v := range spl {
		spl[i] = strings.Split(v, suffix)[0]
	}
	return spl
}

func (w *Window) GetMousePosition() mgl32.Vec2 {
	x, y := w.gl.GetCursorPos()
	wi, he := w.gl.GetSize()
	x32 := ((float32(x) / float32(wi)) - 0.5) * 2
	y32 := -((float32(y) / float32(he)) - 0.5) * 2
	return mgl32.Vec2{x32, y32}
}

//fragmentShader must have one vec4 output
func NewProgram(vertexShader, geometryShader, fragmentShader string) *Program {
	p := &Program{}
	p.vertexShader = vertexShader
	p.geometryShader = geometryShader
	p.fragmentShader = fragmentShader
	p.uniforms = make(map[string]int32)
	p.attributes = make(map[string]uint32)
	return p
}

func (p *Program) Uniform(uniform string) int32 {
	p.compile()
	return p.uniforms[uniform]
}

func (p *Program) Attribute(attribute string) uint32 {
	p.compile()
	return p.attributes[attribute]
}

func (p *Program) Program() uint32 {
	p.compile()
	return p.program
}

func (p *Program) compile() {
	if !p.compiled {
		prg, err := newGLProgram(p.vertexShader, p.geometryShader, p.fragmentShader)
		if err != nil {
			panic(err)
		}
		p.program = prg
		for _, v := range textBetween(p.vertexShader, "uniform ", ";\n") {
			name := strings.Split(v, " ")[1]
			p.uniforms[name] = gl.GetUniformLocation(p.program, gl.Str(name+"\x00"))
		}
		for _, v := range textBetween(p.geometryShader, "uniform ", ";\n") {
			name := strings.Split(v, " ")[1]
			if _, ok := p.uniforms[name]; !ok {
				p.uniforms[name] = gl.GetUniformLocation(p.program, gl.Str(name+"\x00"))
			}
		}
		for _, v := range textBetween(p.fragmentShader, "uniform ", ";\n") {
			name := strings.Split(v, " ")[1]
			if _, ok := p.uniforms[name]; !ok {
				p.uniforms[name] = gl.GetUniformLocation(p.program, gl.Str(name+"\x00"))
			}
		}
		for _, v := range textBetween(p.vertexShader, "in ", ";\n") {
			kindAndName := strings.Split(v, " ")
			// kind := kindAndName[0]
			name := kindAndName[1]
			p.attributes[name] = uint32(gl.GetAttribLocation(p.program, gl.Str(name+"\x00")))
		}
		out := textBetween(p.fragmentShader, "out vec4 ", ";\n")
		if len(out) != 1 {
			panic("exactly one vec4 output must exist in the fragment shader")
		}
		gl.BindFragDataLocation(p.program, 0, gl.Str(out[0]+"\x00"))
		p.compiled = true
	}
}

func newGLProgram(vertexShaderSource, geometryShaderSource, fragmentShaderSource string) (uint32, error) {
	vertexShader, err := compileShader(vertexShaderSource+"\x00", gl.VERTEX_SHADER)
	if err != nil {
		return 0, err
	}

	fragmentShader, err := compileShader(fragmentShaderSource+"\x00", gl.FRAGMENT_SHADER)
	if err != nil {
		return 0, err
	}
	var geometryShader uint32
	if geometryShaderSource != "" {
		geometryShader, err = compileShader(geometryShaderSource+"\x00", gl.GEOMETRY_SHADER)
		if err != nil {
			return 0, err
		}
	}

	program := gl.CreateProgram()

	gl.AttachShader(program, vertexShader)
	if geometryShaderSource != "" {
		gl.AttachShader(program, geometryShader)
	}
	gl.AttachShader(program, fragmentShader)
	gl.LinkProgram(program)

	var status int32
	gl.GetProgramiv(program, gl.LINK_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetProgramiv(program, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetProgramInfoLog(program, logLength, nil, gl.Str(log))

		return 0, fmt.Errorf("failed to link program: %v", log)
	}

	gl.DeleteShader(vertexShader)
	if geometryShaderSource != "" {
		gl.DeleteShader(geometryShader)
	}
	gl.DeleteShader(fragmentShader)
	return program, nil
}

func compileShader(source string, shaderType uint32) (uint32, error) {
	shader := gl.CreateShader(shaderType)

	csources, free := gl.Strs(source)
	gl.ShaderSource(shader, 1, csources, nil)
	free()
	gl.CompileShader(shader)

	var status int32
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetShaderInfoLog(shader, logLength, nil, gl.Str(log))

		return 0, fmt.Errorf("failed to compile %v: %v", source, log)
	}

	return shader, nil
}

func NewImage(file string) (image.Image, error) {
	imgFile, err := os.Open(file)
	if err != nil {
		return nil, fmt.Errorf("texture %q not found on disk: %v", file, err)
	}
	img, _, err := image.Decode(imgFile)
	if err != nil {
		return img, err
	}
	return img, nil
}

func newGLTexture(img image.Image, linearFilter bool, wrap TextureOverflowOperation) (uint32, error) {
	bb := img.Bounds()
	if _, ok := img.(*image.Uniform); ok {
		bb = image.Rect(0, 0, 1, 1)
	}
	rgba := image.NewRGBA(bb)
	if rgba.Stride != rgba.Rect.Size().X*4 {
		return 0, fmt.Errorf("unsupported stride")
	}
	draw.Draw(rgba, rgba.Bounds(), img, image.Point{0, 0}, draw.Src)

	var texture uint32
	gl.GenTextures(1, &texture)
	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, texture)
	if linearFilter {
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	} else {
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
	}
	if wrap == Mirror {
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.MIRRORED_REPEAT)
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.MIRRORED_REPEAT)
	} else if wrap == Repeat {
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT)
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT)
	} else {
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_BORDER)
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_BORDER)
	}
	gl.TexImage2D(
		gl.TEXTURE_2D,
		0,
		gl.RGBA,
		int32(rgba.Rect.Size().X),
		int32(rgba.Rect.Size().Y),
		0,
		gl.RGBA,
		gl.UNSIGNED_BYTE,
		gl.Ptr(rgba.Pix),
	)

	return texture, nil
}

func ErrorCheck(is ...interface{}) {
	fmt.Println("checking GL error:", is)
	err := gl.GetError()
	if err == 0 {
		return
	}
	fmt.Println("GL Error:", is)
	fmt.Println("Error code:", err)
	panic("Check Failed")
}

func newGLTextureFromRGBA(rgba *image.RGBA, linearFilter bool, wrap TextureOverflowOperation) (uint32, error) {

	if rgba.Stride != rgba.Rect.Size().X*4 {
		return 0, fmt.Errorf("unsupported stride")
	}

	var texture uint32
	gl.GenTextures(1, &texture)
	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, texture)
	if linearFilter {
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	} else {
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
	}

	if wrap == Mirror {
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.MIRRORED_REPEAT)
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.MIRRORED_REPEAT)
	} else if wrap == Repeat {
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT)
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT)
	} else {
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_BORDER)
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_BORDER)
	}

	gl.TexImage2D(
		gl.TEXTURE_2D,
		0,
		gl.RGBA,
		int32(rgba.Rect.Size().X),
		int32(rgba.Rect.Size().Y),
		0,
		gl.RGBA,
		gl.UNSIGNED_BYTE,
		gl.Ptr(rgba.Pix),
	)

	return texture, nil
}

// var Window *glfw.Window

// var WindowSize geometry.Vector

// var OnResize func(size geometry.Vector)

var created bool

func CreateWindow(title string, size geometry.Vector, f func(w *Window) bool) {
	if created {
		//TODO (low priority) support more than one window
		panic("creating more than one window is unsupported")
	} else {
		created = true
	}
	w := &Window{}
	runtime.LockOSThread()
	if err := glfw.Init(); err != nil {
		panic(err)
	}

	glfw.WindowHint(glfw.Resizable, glfw.True)
	glfw.WindowHint(glfw.ContextVersionMajor, 3)
	glfw.WindowHint(glfw.ContextVersionMinor, 3)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)

	// glfw.WindowHint(glfw.Samples, 8)
	// w.OnResize(nil)
	var err error
	w.gl, err = glfw.CreateWindow(int(size.X()), int(size.Y()), title, nil, nil)
	if err != nil {
		panic(err)
	}

	// w.gl.SetSizeCallback(func(window *glfw.Window, width, height int) {
	// 	w.size = geometry.Vector{float32(width), float32(height)}
	// })
	// OnResize(WindowSize)
	// Window.SetSizeCallback(func(window *glfw.Window, w, h int) {
	// WindowSize = geometry.Vector{float32(w), float32(h)}
	// OnResize(WindowSize)
	// })
	w.gl.MakeContextCurrent()

	// glfw.SwapInterval(1)

	// GetWindowSize = func() mgl32.Vec2 {
	// 	w, h := Window.GetSize()
	// 	return mgl32.Vec2{float32(w), float32(h)}
	// }
	// Initialize Glow
	if err = gl.Init(); err != nil {
		panic(err)
	}

	// version := gl.GoStr(gl.GetString(gl.VERSION))
	// fmt.Println("OpenGL version", version)

	// Configure global settings
	gl.ClearColor(0, 0, 0, 0)
	// defer os.Exit(0)
	defer glfw.Terminate()
	for !w.gl.ShouldClose() {
		frames++
		glfw.PollEvents()
		if !f(w) {
			break
		}
		w.gl.SwapBuffers()
	}
}

//TODO put a window inside canvas

//
// func (w *Window) OnResize(f func(size geometry.Vector)) {
// 	f(w.size)
// 	w.onResizeF = f
// 	w.gl.SetSizeCallback(func(window *glfw.Window, width, height int) {
// 		w.size = geometry.Vector{float32(width), float32(height)}
// 		if w.onResizeF != nil {
// 			w.onResizeF(w.size)
// 		}
// 	})
// }

func ClearColour() {
	gl.Clear(gl.COLOR_BUFFER_BIT)
}

func ClearColourAndDepth() {
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
}

func ClearDepth() {
	gl.Clear(gl.DEPTH_BUFFER_BIT)
}

var frames int

func init() {
	ticker := time.NewTicker(time.Second)
	go func() {
		for _ = range ticker.C {
			fmt.Println("tick...", frames)
			frames = 0
		}
	}()
}

type Window struct {
	gl *glfw.Window
}

func (w *Window) KeyIsPressed(k glfw.Key) bool {
	return w.gl.GetKey(k) == glfw.Press
}
func (w *Window) MouseButtonIsPressed(k glfw.MouseButton) bool {
	return w.gl.GetMouseButton(k) == glfw.Press
}

func (w *Window) Size() geometry.Vector {
	wi, he := w.gl.GetSize()
	return geometry.Vector{float32(wi), float32(he)}
}
